$(document).ready(function() {
    const urlParams = new URLSearchParams(window.location.search);
    const postId = urlParams.get('id');

    fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`)
        .then(response => response.json())
        .then(post => {
            $('#title').val(post.title);
            $('#body').val(post.body);
        })
        .catch(error => console.error('Error fetching post:', error));

    $('#edit-post-form').submit(function(e) {
        e.preventDefault();
        const title = $('#title').val();
        const body = $('#body').val();
        fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
            method: 'PUT',
            body: JSON.stringify({
                id: postId,
                title: title,
                body: body,
                userId: 1
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response => {
            if (response.ok) {
                alert('Post updated successfully');
                window.location.href = 'index.html';
            } else {
                throw new Error('Failed to update post');
            }
        })
        .catch(error => console.error('Error updating post:', error));
    });
});