$(document).ready(function() {
    $('#create-post-form').submit(function(e) {
        e.preventDefault();
        const title = $('#title').val();
        const body = $('#body').val();
        fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify({
                title: title,
                body: body,
                userId: 1
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        })
        .then(response => {
            if (response.ok) {
                alert('Post created successfully');
                window.location.href = 'index.html';
            } else {
                throw new Error('Failed to create post');
            }
        })
        .catch(error => console.error('Error creating post:', error));
    });
});