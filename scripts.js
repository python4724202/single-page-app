$(document).ready(function() {
    // Function to fetch and display all posts
    function fetchPosts() {
        $('#content').empty();
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(posts => {
                posts.forEach(post => {
                    $('#content').append(`
                        <div class="card mb-3">
                            <div class="card-body">
                                <h5 class="card-title">${post.title}</h5>
                                <p class="card-text">${post.body}</p>
                                <button class="btn btn-primary edit-post" data-id="${post.id}">Редактировать</button>
                                <button class="btn btn-danger delete-post" data-id="${post.id}">Удалить</button>
                            </div>
                        </div>
                    `);
                });
            })
            .catch(error => console.error('Error fetching posts:', error));
    }
    function searchPosts(query) {
        $('#content').empty();
        fetch(`https://jsonplaceholder.typicode.com/posts?q=${query}`)
            .then(response => response.json())
            .then(posts => {
                posts.forEach(post => {
                    $('#content').append(`
                        <div class="card mb-3">
                            <div class="card-body">
                                <h5 class="card-title">${post.title}</h5>
                                <p class="card-text">${post.body}</p>
                                <button class="btn btn-primary edit-post" data-id="${post.id}">Редактировать</button>
                                <button class="btn btn-danger delete-post" data-id="${post.id}">Удалить</button>
                            </div>
                        </div>
                    `);
                });
            })
            .catch(error => console.error('Error searching posts:', error));
    }
    function likePost(postId) {
        fetch(`https://jsonplaceholder.typicode.com/posts/${postId}/likes`, {
            method: 'POST'
        })
        .then(response => {
            if (response.ok) {
                // Обновляем отображение поста
                fetchPosts();
            } else {
                throw new Error('Failed to like post');
            }
        })
        .catch(error => console.error('Error liking post:', error));
    }
    
    // Initial fetch of all posts
    fetchPosts();
    
    // Event listener for clicking on Home link
    $('#home').click(function(e) {
        e.preventDefault();
        fetchPosts();
    });
    
    // Event listener for clicking on Create Post link
    $('#create').click(function(e) {
        e.preventDefault();
        $('#content').empty().load('create.html');
    });
    
    // Event listener for clicking on Edit button
    $(document).on('click', '.edit-post', function() {
        const postId = $(this).data('id');
        $('#content').empty().load(`edit.html?id=${postId}`);
    });
    
    // Event listener for clicking on Delete button
    $(document).on('click', '.delete-post', function() {
        const postId = $(this).data('id');
        fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
            method: 'DELETE'
        })
        .then(response => {
            if (response.ok) {
                fetchPosts();
            } else {
                throw new Error('Failed to delete post');
            }
        })
        .catch(error => console.error('Error deleting post:', error));
    });
    $('#search-form').submit(function(e) {
        e.preventDefault();
        const query = $('#search-input').val();
        searchPosts(query);
    });
    $(document).on('click', '.like-btn', function() {
        const postId = $(this).data('id');
        likePost(postId);
    });
});
